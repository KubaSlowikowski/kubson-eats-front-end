export enum RestaurantCategory {
  ORIENT = 'Orient',
  ITALIAN = 'Italian',
  TURKISH = 'Turkish',
  ASIAN = 'Asian',
  JAPAN = 'Japan',
  MEXICAN = 'Mexican'
}
