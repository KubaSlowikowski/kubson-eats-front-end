export enum ProductCategory {
  APPETIZER= 'Appetizer',
  BREAKFAST = 'Breakfast',
  MAIN_DISH = 'Main dish',
  DESSERT = 'Dessert',
  SNACK = 'Snack',
  SOUP = 'Souo',
  DRINK = 'Drink',
  ALCOHOL = 'Alcohol'
}
