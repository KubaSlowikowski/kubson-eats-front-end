export enum OrderStatus {
  IN_DELIVERY = 'In delivery',
  READY = 'Ready',
  IN_PROGRESS = 'In progress',
  WAITING = 'Waiting',
  CLOSED = 'Closed'
}
