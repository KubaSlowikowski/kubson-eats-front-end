import {Component, OnInit, ViewChild} from '@angular/core';
import {Page} from '../../../../entities/page';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {Sort, SortDirection} from '@angular/material/sort';
import {Restaurant} from '../../../../entities/restaurant';
import {Address} from '../../../../entities/address';
import {AddressDetailsComponent} from '../../address/address-details/address-details.component';
import {MatDialog} from '@angular/material/dialog';
import {RestaurantService} from '../../../../services/restaurant/restaurant-service';
import {ProductService} from '../../../../services/product/product-service';
import {Product} from '../../../../entities/product';
import {ProductDetailsComponent} from '../../product/product-details/product-details.component';
import {ConfirmDialogComponent} from '../../../confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant-table.component.html',
  styleUrls: ['./restaurant-table.component.css']
})
export class RestaurantTableComponent implements OnInit {
  columnsToDisplay: string[] = ['id', 'name', 'category', 'addressId', 'email', 'phoneNumber', 'login', 'products', 'options'];
  dataSource: Restaurant[] = [];
  page: Page<Restaurant> = new Page();
  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: number;
  length: number;
  sort: Sort = new class implements Sort {
    active = 'id';
    direction: SortDirection = 'asc';
  };
  searchParam: string;
  productsMap: Map<number, boolean> = new Map<number, boolean>();
  products: Product[] = [];

  constructor(
    private restaurantService: RestaurantService,
    private productService: ProductService,
    private dialog: MatDialog
  ) {
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit(): void {
    this.page.size = 10;
    this.page.number = 0;
    this.refreshData();
  }

  refreshData(event?: PageEvent): PageEvent {
    if (event) {
      this.page.number = event.pageIndex;
      this.page.size = event.pageSize;
    }

    if (this.sort.direction === '') {
      this.page.isSorted = false;
    } else {
      this.page.isSorted = true;
    }
    this.page.sortedBy = this.sort.active;
    this.page.dir = this.sort.direction;

    this.restaurantService.getAll(this.page, this.searchParam).subscribe(response => {
        this.page.fromResponse(response);
        this.dataSource = response.content;
        this.pageIndex = response.number;
        this.pageSize = response.size;
        this.length = response.totalElements;
      }
    );
    return event;
  }

  sortData(sort: Sort): void {
    this.sort = sort;
    this.refreshData();
  }

  applyFilter(event: KeyboardEvent): void {
    if (event.key === 'Enter') {
      const filterValue = (event.target as HTMLInputElement).value;
      this.refreshData();
      this.searchParam = '&search='.concat(filterValue);
      this.restaurantService.getAll(this.page, this.searchParam).subscribe(response => {
          this.dataSource = response.content;
        }
      );
    }
  }

  deleteRestaurant(restaurant: Restaurant) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        data: {
          message: 'delete this restaurant'
        }
      }
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.restaurantService.delete(restaurant.id).subscribe(
          () => {
            this.refreshData();
          }
        );
      }
    });
  }

  openAddressDetails(address: Address) {
    this.dialog.open(AddressDetailsComponent, {
      data: {
        id: address.id,
        country: address.country,
        city: address.city,
        street: address.street,
        buildingNumber: address.buildingNumber,
        apartmentNumber: address.apartmentNumber,
        zipCode: address.zipCode
      }
    });
  }

  hideProduct(id: number) {
    this.productsMap.set(id, false);
  }

  getProductsByRestaurantId(id: number): void {
    this.productService.getByRestaurantId(new Page(1000), id).subscribe(
      (receivedProducts) => {
        this.products = receivedProducts.content;
        this.refreshData();
      }
    );
    this.productsMap.clear();
    this.productsMap.set(id, true);
  }

  openProductDetails(product: Product) {
    this.dialog.open(ProductDetailsComponent, {
      data: {
        id: product.id,
        name: product.name,
        price: product.price,
        description: product.description,
        restaurantId: product.restaurantId,
        position: product.position,
        category: product.category
      }
    });
  }
}
