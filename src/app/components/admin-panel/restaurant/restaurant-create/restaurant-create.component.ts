import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {RestaurantService} from '../../../../services/restaurant/restaurant-service';
import {Restaurant} from '../../../../entities/restaurant';
import {RestaurantCategory} from "../../../../utils/restaurant-category.enum";

@Component({
  selector: 'app-restaurant-create',
  templateUrl: './restaurant-create.component.html',
  styleUrls: ['./restaurant-create.component.css']
})
export class RestaurantCreateComponent implements OnInit {
  createRestaurantForm: FormGroup;
  submitted = false;
  categories = RestaurantCategory;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private restaurantService: RestaurantService,
    private location: Location,
  ) {
  }

  ngOnInit(): void {
    this.createRestaurantForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(255)]],
      category: ['', [Validators.required, Validators.maxLength(255)]],
      login: ['', [Validators.required, Validators.maxLength(255)]],
      password: ['', [Validators.required, Validators.maxLength(255)]],
      email: ['', [Validators.required, Validators.maxLength(255)]],
      phoneNumber: ['', [Validators.required, Validators.maxLength(255)]],
      country: ['', [Validators.required, Validators.maxLength(255)]],
      city: ['', [Validators.required, Validators.maxLength(255)]],
      street: ['', [Validators.required, Validators.maxLength(255)]],
      buildingNumber: ['', [Validators.required, Validators.min(0)]],
      apartmentNumber: ['', [Validators.min(0)]],
      zipCode: ['', [Validators.required, Validators.maxLength(6)]]
    });
  }

  submitForm() {
    this.submitted = true;
    if (this.createRestaurantForm.invalid) {
      return;
    }
    const newRestaurant = this.convertFormToRestaurant(this.createRestaurantForm);
    this.restaurantService.save(newRestaurant).subscribe(
      () => {
        this.goBack();
      }
    );
  }

  convertFormToRestaurant(restaurantForm: FormGroup): Restaurant {
    return {
      id: null,
      name: restaurantForm.value.name,
      category: restaurantForm.value.category,
      login: restaurantForm.value.login,
      password: restaurantForm.value.password,
      phoneNumber: restaurantForm.value.phoneNumber,
      email: restaurantForm.value.email,
      address: {
        id: null,
        country: restaurantForm.value.country,
        city: restaurantForm.value.city,
        street: restaurantForm.value.street,
        buildingNumber: restaurantForm.value.buildingNumber,
        apartmentNumber: restaurantForm.value.apartmentNumber,
        zipCode: restaurantForm.value.zipCode
      },
      products: null //FIXME
    };
  }

  goBack() {
    this.location.back();
  }

}
