import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {RestaurantService} from '../../../../services/restaurant/restaurant-service';
import {Product} from '../../../../entities/product';
import {Restaurant} from '../../../../entities/restaurant';
import {ProductService} from '../../../../services/product/product-service';
import {Page} from '../../../../entities/page';
import {RestaurantCategory} from "../../../../utils/restaurant-category.enum";

@Component({
  selector: 'app-restaurant-update',
  templateUrl: './restaurant-update.component.html',
  styleUrls: ['./restaurant-update.component.css',],
  encapsulation: ViewEncapsulation.None
})
export class RestaurantUpdateComponent implements OnInit {
  editRestaurantForm: FormGroup;
  isFormSubmitted = false;
  id: number;
  addressId: number;
  availableProducts: Product[];
  selectedProducts: Product[];
  categories = RestaurantCategory;

  constructor(
    private formBuilder: FormBuilder,
    private restaurantService: RestaurantService,
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.editRestaurantForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(255)]],
      category: ['', [Validators.required, Validators.maxLength(255)]],
      login: ['', [Validators.required, Validators.maxLength(255)]],
      password: ['', [Validators.required, Validators.maxLength(255)]],
      email: ['', [Validators.required, Validators.maxLength(255)]],
      phoneNumber: ['', [Validators.required, Validators.maxLength(255)]],
      products: ['', []],
      country: ['', [Validators.required, Validators.maxLength(255)]],
      city: ['', [Validators.required, Validators.maxLength(255)]],
      street: ['', [Validators.required, Validators.maxLength(255)]],
      buildingNumber: ['', [Validators.required, Validators.min(0)]],
      apartmentNumber: ['', [Validators.min(0)]],
      zipCode: ['', [Validators.required, Validators.maxLength(6)]]
    });

    this.activatedRoute.paramMap.subscribe(params => {
      this.id = +params.get('id');
      this.restaurantService.getById(this.id).subscribe(
        receivedRestaurant => {
          this.availableProducts = receivedRestaurant.products;
          this.addressId = receivedRestaurant.address.id;
          this.editRestaurantForm.setValue({
            name: receivedRestaurant.name,
            category: receivedRestaurant.category,
            login: receivedRestaurant.login,
            // password: receivedRestaurant.password, FIXME
            password: null,
            phoneNumber: receivedRestaurant.phoneNumber,
            email: receivedRestaurant.email,
            products: receivedRestaurant.products,
            country: receivedRestaurant.address.country,
            city: receivedRestaurant.address.city,
            street: receivedRestaurant.address.street,
            buildingNumber: receivedRestaurant.address.buildingNumber,
            apartmentNumber: receivedRestaurant.address.apartmentNumber,
            zipCode: receivedRestaurant.address.zipCode,
          });
        }
      );
    });

    this.productService.getByRestaurantId(new Page<Product>(1000), this.id).subscribe(
      response => {
        this.availableProducts = response.content;
      }
    );
  }

  submitForm() {
    this.isFormSubmitted = true;
    if (this.editRestaurantForm.invalid) {
      return;
    }

    const restaurantToUpdate = this.convertFormToRestaurant(this.editRestaurantForm);

    this.restaurantService.update(restaurantToUpdate).subscribe(
      () => {
        this.router.navigate(['/admin-panel/restaurants']);
      }
    );
  }

  convertFormToRestaurant(restaurantForm: FormGroup): Restaurant {
    return {
      id: this.id,
      name: restaurantForm.value.name,
      category: restaurantForm.value.category,
      login: restaurantForm.value.login,
      password: restaurantForm.value.password,
      phoneNumber: restaurantForm.value.phoneNumber,
      email: restaurantForm.value.email,
      products: restaurantForm.value.products,
      address: {
        id: null,
        country: restaurantForm.value.country,
        city: restaurantForm.value.city,
        street: restaurantForm.value.street,
        buildingNumber: restaurantForm.value.buildingNumber,
        apartmentNumber: restaurantForm.value.apartmentNumber,
        zipCode: restaurantForm.value.zipCode
      },
    };
  }

  goBack() {
    this.location.back();
  }

  compareProducts(p1: Product, p2: Product): boolean { // used to preselect values in <mat-select> element
    return p1 && p2 ? p1.id === p2.id : p1 === p2;
  }
}
