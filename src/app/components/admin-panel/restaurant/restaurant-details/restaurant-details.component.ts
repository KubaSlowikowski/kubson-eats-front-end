import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Restaurant} from "../../../../entities/restaurant";

@Component({
  selector: 'app-restaurant-details',
  templateUrl: './restaurant-details.component.html',
  styleUrls: ['./restaurant-details.component.css']
})
export class RestaurantDetailsComponent {

  constructor(
    private dialogRef: MatDialogRef<RestaurantDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public restaurant: Restaurant
  ) { }


  closeDialog() {
    this.dialogRef.close();
  }

}
