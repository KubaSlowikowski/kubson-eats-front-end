import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {ProductService} from '../../../../services/product/product-service';
import {Product} from '../../../../entities/product';
import {Restaurant} from "../../../../entities/restaurant";
import {RestaurantService} from "../../../../services/restaurant/restaurant-service";
import {Page} from "../../../../entities/page";
import {ProductCategory} from "../../../../utils/product-category.enum";

@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ProductUpdateComponent implements OnInit {
  editProductForm: FormGroup;
  isFormSubmitted = false;
  id: number;
  availableRestaurants: Restaurant[] = [];
  categories = ProductCategory

  constructor(
    private formBuilder: FormBuilder,
    private productService: ProductService,
    private restaurantService: RestaurantService,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.editProductForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(255)]],
      description: ['', [Validators.required, Validators.maxLength(255)]],
      price: ['', [Validators.required, Validators.min(0)]],
      restaurantId: ['', [Validators.required, Validators.min(0)]],
      position: ['', [Validators.min(1)]],
      category: ['', [Validators.required, Validators.maxLength(255)]],
    });

    this.activatedRoute.paramMap.subscribe(params => {
      this.id = +params.get('id');
      this.productService.getById(this.id).subscribe(
        receivedProduct => {
          this.editProductForm.setValue({
            name: receivedProduct.name,
            description: receivedProduct.description,
            price: receivedProduct.price,
            restaurantId: receivedProduct.restaurantId,
            position: receivedProduct.position,
            category: receivedProduct.category
          });
        }
      );
    });

    this.restaurantService.getAll(new Page<Restaurant>(1000, 'name')).subscribe(
      response => {
        this.availableRestaurants = response.content;
      }
    );
  }

  submitForm() {
    this.isFormSubmitted = true;
    if (this.editProductForm.invalid) {
      return;
    }

    const userToUpdate = this.convertFormToProduct(this.editProductForm);

    this.productService.update(userToUpdate).subscribe(
      () => {
        this.router.navigate(['/products']);

      }
    );
  }

  convertFormToProduct(productForm: FormGroup): Product {
    return {
      id: this.id,
      name: productForm.value.name,
      description: productForm.value.description,
      price: productForm.value.price,
      restaurantId: productForm.value.restaurantId,
      position: productForm.value.position,
      category: productForm.value.category,
    };
  }

  goBack() {
    this.location.back();
  }

}
