import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {ProductService} from '../../../../services/product/product-service';
import {Product} from '../../../../entities/product';
import {Restaurant} from "../../../../entities/restaurant";
import {RestaurantService} from "../../../../services/restaurant/restaurant-service";
import {Page} from "../../../../entities/page";
import {ProductCategory} from "../../../../utils/product-category.enum";

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ProductCreateComponent implements OnInit {
  createProductForm: FormGroup;
  submitted = false;
  availableRestaurants: Restaurant[] = [];
  categories = ProductCategory

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private productService: ProductService,
    private restaurantService: RestaurantService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.createProductForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(255)]],
      description: ['', [Validators.required, Validators.maxLength(255)]],
      price: ['', [Validators.required, Validators.min(0)]],
      restaurantId: ['', [Validators.required, Validators.min(0)]],
      position: ['', [Validators.min(1)]],
      category: ['', [Validators.required, Validators.maxLength(255)]],
    });

    this.restaurantService.getAll(new Page<Restaurant>(1000, 'name')).subscribe(
      response => {
        this.availableRestaurants = response.content;
      }
    );
  }

  submitForm() {
    this.submitted = true;
    if (this.createProductForm.invalid) {
      return;
    }
    const newProduct = this.convertFormToUser(this.createProductForm);
    this.productService.save(newProduct).subscribe(
      () => {
        this.router.navigate(['/admin-panel/products']);
      }
    );
  }

  convertFormToUser(productForm: FormGroup): Product {
    return {
      id: null,
      name: productForm.value.name,
      description: productForm.value.description,
      price: productForm.value.price,
      restaurantId: productForm.value.restaurantId,
      position: productForm.value.position,
      category: productForm.value.category,
    };
  }

  goBack() {
    this.location.back();
  }
}
