import {Component, OnInit, ViewChild} from '@angular/core';
import {User} from '../../../../entities/user';
import {Page} from '../../../../entities/page';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {Sort, SortDirection} from '@angular/material/sort';
import {Product} from '../../../../entities/product';
import {ProductService} from '../../../../services/product/product-service';
import {ConfirmDialogComponent} from '../../../confirm-dialog/confirm-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {RestaurantService} from "../../../../services/restaurant/restaurant-service";
import {RestaurantDetailsComponent} from "../../restaurant/restaurant-details/restaurant-details.component";

@Component({
  selector: 'app-product',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.css']
})
export class ProductTableComponent implements OnInit {

  columnsToDisplay: string[] = ['id', 'name', 'price', 'description', 'restaurantId', 'position', 'category', 'options'];
  dataSource: Product[] = [];
  page: Page<Product> = new Page();
  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: number;
  length: number;
  sort: Sort = new class implements Sort {
    active = 'id';
    direction: SortDirection = 'asc';
  };
  searchParam: string;

  constructor(
    private productService: ProductService,
    private restaurantService: RestaurantService,
    private dialog: MatDialog
  ) {
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;


  ngOnInit(): void {
    this.page.size = 10;
    this.page.number = 0;
    this.refreshData();
  }

  refreshData(event?: PageEvent): PageEvent {
    if (event) {
      this.page.number = event.pageIndex;
      this.page.size = event.pageSize;
    }

    if (this.sort.direction === '') {
      this.page.isSorted = false;
    } else {
      this.page.isSorted = true;
    }
    this.page.sortedBy = this.sort.active;
    this.page.dir = this.sort.direction;

    this.productService.getAll(this.page, this.searchParam).subscribe(response => {
        this.page.fromResponse(response);
        this.dataSource = response.content;
        this.pageIndex = response.number;
        this.pageSize = response.size;
        this.length = response.totalElements;
      }
    );
    return event;
  }

  sortData(sort: Sort): void {
    this.sort = sort;
    this.refreshData();
  }

  applyFilter(event: KeyboardEvent): void {
    if (event.key === 'Enter') {
      const filterValue = (event.target as HTMLInputElement).value;
      this.refreshData();
      this.searchParam = '&search='.concat(filterValue);
      this.productService.getAll(this.page, this.searchParam).subscribe(response => {
          this.dataSource = response.content;
        }
      );
    }
  }

  deleteUser(user: User) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        data: {
          message: 'delete this product'
        }
      }
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.productService.delete(user.id).subscribe(
          () => {
            this.refreshData();
          }
        );
      }
    });
  }

  openRestaurantDetails(restaurantId: number) {
    let restaurant;
    this.restaurantService.getById(restaurantId).subscribe(response => {
      restaurant = response;
      this.dialog.open(RestaurantDetailsComponent, {
        data: {
          id: restaurant.id,
          name: restaurant.name,
          category: restaurant.category,
          address: restaurant.address,
          phoneNumber: restaurant.phoneNumber,
          email: restaurant.email,
          login: restaurant.login,
          password: restaurant.password,
          products: restaurant.products
        }
      });
    });
  }
}
