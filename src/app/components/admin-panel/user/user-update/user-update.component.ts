import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../../services/user/user-service';
import {User} from '../../../../entities/user';
import {Location} from '@angular/common';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent implements OnInit {

  editUserForm: FormGroup;
  isFormSubmitted = false;
  id: number;
  addressId: number;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private router: Router,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.editUserForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(255)]],
      secondName: ['', [Validators.required, Validators.maxLength(255)]],
      login: ['', [Validators.required, Validators.maxLength(255)]],
      password: ['', [Validators.required, Validators.maxLength(255)]],
      email: ['', [Validators.required, Validators.maxLength(255)]],
      country: ['', [Validators.required, Validators.maxLength(255)]],
      city: ['', [Validators.required, Validators.maxLength(255)]],
      street: ['', [Validators.required, Validators.maxLength(255)]],
      buildingNumber: ['', [Validators.required, Validators.min(0)]],
      apartmentNumber: ['', [Validators.min(0)]],
      zipCode: ['', [Validators.required, Validators.maxLength(6)]],
      phoneNumber: ['', [Validators.required, Validators.maxLength(9)]],
    });

    this.activatedRoute.paramMap.subscribe(params => {
      this.id = +params.get('id');
      this.userService.getById(this.id).subscribe(
        receivedUser => {
          this.addressId = receivedUser.address.id;
          this.editUserForm.setValue({
            name: receivedUser.name,
            secondName: receivedUser.secondName,
            login: receivedUser.login,
            // password: receivedUser.password, FIXME
            password: null,
            email: receivedUser.email,
            country: receivedUser.address.country,
            city: receivedUser.address.city,
            street: receivedUser.address.street,
            buildingNumber: receivedUser.address.buildingNumber,
            apartmentNumber: receivedUser.address.apartmentNumber,
            zipCode: receivedUser.address.zipCode,
            phoneNumber: receivedUser.phoneNumber
          });
        }
      );
    });
  }

  submitForm() {
    this.isFormSubmitted = true;
    if (this.editUserForm.invalid) {
      return;
    }

    const userToUpdate = this.convertFormToUser(this.editUserForm);

    this.userService.update(userToUpdate).subscribe(
      () => {
        this.router.navigate(['/admin-panel/users']);

      }
    );
    // if (!this.editUserForm.invalid) { todo
    //   this._snackBar.open('user updated successfully', '',{
    //     duration: 3 * 1000,
    //   });
    // }

  }

  convertFormToUser(userForm: FormGroup): User {
    return {
      id: this.id,
      name: userForm.value.name,
      secondName: userForm.value.secondName,
      login: userForm.value.login,
      password: userForm.value.password,
      email: userForm.value.email,
      address: {
        id: this.addressId,
        country: userForm.value.country,
        city: userForm.value.city,
        street: userForm.value.street,
        buildingNumber: userForm.value.buildingNumber,
        apartmentNumber: userForm.value.apartmentNumber,
        zipCode: userForm.value.zipCode
      },
      phoneNumber: userForm.value.phoneNumber
    };
  }

  goBack() {
    this.location.back();
  }
}
