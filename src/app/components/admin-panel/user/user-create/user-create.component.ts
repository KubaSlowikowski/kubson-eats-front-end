import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {UserService} from '../../../../services/user/user-service';
import {User} from '../../../../entities/user';
import {Location} from '@angular/common';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {
  createUserForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.createUserForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(255)]],
      secondName: ['', [Validators.required, Validators.maxLength(255)]],
      login: ['', [Validators.required, Validators.maxLength(255)]],
      password: ['', [Validators.required, Validators.maxLength(255)]], //FIXME
      email: ['', [Validators.required, Validators.maxLength(255)]],
      country: ['', [Validators.required, Validators.maxLength(255)]],
      city: ['', [Validators.required, Validators.maxLength(255)]],
      street: ['', [Validators.required, Validators.maxLength(255)]],
      buildingNumber: ['', [Validators.required, Validators.min(0)]],
      apartmentNumber: ['', [Validators.min(0)]],
      zipCode: ['', [Validators.required, Validators.maxLength(6)]],
      phoneNumber: ['', [Validators.required, Validators.maxLength(9)]],
    });
  }

  submitForm() {
    this.submitted = true;
    if (this.createUserForm.invalid) {
      return;
    }
    const newUser = this.convertFormToUser(this.createUserForm);
    this.userService.save(newUser).subscribe(
      () => {
        this.goBack();
      }
    );
  }

  convertFormToUser(userForm: FormGroup): User {
    return {
      id: null,
      name: userForm.value.name,
      login: userForm.value.login,
      password: userForm.value.password,
      secondName: userForm.value.secondName,
      email: userForm.value.email,
      address: {
        id: null,
        country: userForm.value.country,
        city: userForm.value.city,
        street: userForm.value.street,
        buildingNumber: userForm.value.buildingNumber,
        apartmentNumber: userForm.value.apartmentNumber,
        zipCode: userForm.value.zipCode
      },
      phoneNumber: userForm.value.phoneNumber
    };
  }

  goBack() {
    this.location.back();
  }

}
