import {Component, OnInit, ViewChild} from '@angular/core';
import {User} from '../../../../entities/user';
import {Page} from '../../../../entities/page';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {Sort, SortDirection} from '@angular/material/sort';
import {UserService} from '../../../../services/user/user-service';
import {MatDialog} from '@angular/material/dialog';
import {Address} from '../../../../entities/address';
import {AddressDetailsComponent} from '../../address/address-details/address-details.component';
import {ConfirmDialogComponent} from '../../../confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-user',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})
export class UserTableComponent implements OnInit {
  columnsToDisplay: string[] = ['id', 'name', 'secondName', 'login', 'email', 'addressId', 'phoneNumber', 'options'];
  dataSource: User[] = [];
  page: Page<User> = new Page();
  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: number;
  length: number;
  sort: Sort = new class implements Sort {
    active = 'id';
    direction: SortDirection = 'asc';
  };
  searchParam: string;

  constructor(
    private userService: UserService,
    private dialog: MatDialog
  ) {
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit(): void {
    this.page.size = 10;
    this.page.number = 0;
    this.refreshData();
  }

  refreshData(event?: PageEvent): PageEvent {
    if (event) {
      this.page.number = event.pageIndex;
      this.page.size = event.pageSize;
    }

    if (this.sort.direction === '') {
      this.page.isSorted = false;
    } else {
      this.page.isSorted = true;
    }
    this.page.sortedBy = this.sort.active;
    this.page.dir = this.sort.direction;

    this.userService.getAll(this.page, this.searchParam).subscribe(response => {
        this.page.fromResponse(response);
        this.dataSource = response.content;
        this.pageIndex = response.number;
        this.pageSize = response.size;
        this.length = response.totalElements;
      }
    );
    return event;
  }

  sortData(sort: Sort): void {
    this.sort = sort;
    this.refreshData();
  }

  applyFilter(event: KeyboardEvent): void {
    if (event.key === 'Enter') {
      const filterValue = (event.target as HTMLInputElement).value;
      this.refreshData();
      this.searchParam = '&search='.concat(filterValue);
      this.userService.getAll(this.page, this.searchParam).subscribe(response => {
          this.dataSource = response.content;
        }
      );
    }
  }

  deleteUser(user: User) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        data: {
          message: 'delete this user'
        }
      }
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.userService.delete(user.id).subscribe(
          () => {
            this.refreshData();
          }
        );
      }
    });
  }

  openAddressDetails(address: Address) {
    this.dialog.open(AddressDetailsComponent, {
      data: {
        id: address.id,
        country: address.country,
        city: address.city,
        street: address.street,
        buildingNumber: address.buildingNumber,
        apartmentNumber: address.apartmentNumber,
        zipCode: address.zipCode
      }
    });
  }
}
