import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Product} from '../../../../entities/product';
import {User} from '../../../../entities/user';
import {OrderService} from '../../../../services/order/order-service';
import {UserService} from '../../../../services/user/user-service';
import {ProductService} from '../../../../services/product/product-service';
import {Page} from '../../../../entities/page';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {Order} from '../../../../entities/order';
import {OrderStatus} from "../../../../utils/order-status.enum";

@Component({
  selector: 'app-order-update',
  templateUrl: './order-update.component.html',
  styleUrls: ['./order-update.component.css']
})
export class OrderUpdateComponent implements OnInit {
  editOrderForm: FormGroup;
  isFormSubmitted = false;
  id: number;
  availableProducts: Product[];
  availableUsers: User[];
  orderStatus = OrderStatus;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private orderService: OrderService,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private productService: ProductService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.editOrderForm = this.formBuilder.group({
      orderComment: ['', [Validators.maxLength(255)]],
      userId: ['', [Validators.required, Validators.min(0)]],
      products: ['', [Validators.required]],
      orderStatus: ['', [Validators.required, Validators.maxLength(255)]]
    });

    this.activatedRoute.paramMap.subscribe(params => {
      this.id = +params.get('id');
      this.orderService.getById(this.id).subscribe(
        receivedOrder => {
          this.editOrderForm.setValue({
            orderComment: receivedOrder.orderComment,
            userId: receivedOrder.userId,
            products: receivedOrder.products,
            orderStatus: receivedOrder.orderStatus
          });
        }
      );
    });

    this.productService.getAll(new Page<Product>(1000)).subscribe(
      response => {
        this.availableProducts = response.content;
      }
    );

    this.userService.getAll(new Page<User>(1000)).subscribe(
      response => {
        this.availableUsers = response.content;
      }
    )
  }

  submitForm() {
    this.isFormSubmitted = true;
    if (this.editOrderForm.invalid) {
      return;
    }

    const orderToUpdate = this.convertFormToOrder(this.editOrderForm);

    this.orderService.update(orderToUpdate).subscribe(
      () => {
        this.router.navigate(['/orders']);

      }
    );
  }

  convertFormToOrder(orderForm: FormGroup): Order {
    return {
      id: this.id,
      orderComment: orderForm.value.orderComment,
      userId: orderForm.value.userId,
      products: orderForm.value.products,
      orderStatus: orderForm.value.orderStatus
    };
  }

  goBack() {
    this.location.back();
  }

  compareProducts(p1: Product, p2: Product): boolean { // used to preselect values in <mat-select> element
    return p1 && p2 ? p1.id === p2.id : p1 === p2;
  }
}
