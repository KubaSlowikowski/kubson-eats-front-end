import {Component, OnInit, ViewChild} from '@angular/core';
import {Page} from '../../../../entities/page';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {Sort, SortDirection} from '@angular/material/sort';
import {Order} from '../../../../entities/order';
import {MatDialog} from '@angular/material/dialog';
import {OrderService} from '../../../../services/order/order-service';
import {ConfirmDialogComponent} from '../../../confirm-dialog/confirm-dialog.component';
import {Product} from '../../../../entities/product';
import {ProductDetailsComponent} from '../../product/product-details/product-details.component';
import {ProductService} from '../../../../services/product/product-service';
import {UserService} from '../../../../services/user/user-service';
import {UserDetailsComponent} from '../../user/user-details/user-details.component';

@Component({
  selector: 'app-order',
  templateUrl: './order-table.component.html',
  styleUrls: ['./order-table.component.css']
})
export class OrderTableComponent implements OnInit {

  columnsToDisplay: string[] = ['id', 'orderComment', 'userId', 'products', 'orderStatus', 'options'];
  dataSource: Order[] = [];
  page: Page<Order> = new Page();
  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: number;
  length: number;
  sort: Sort = new class implements Sort {
    active = 'id';
    direction: SortDirection = 'asc';
  };
  searchParam: string;
  productsMap: Map<number, boolean> = new Map<number, boolean>();

  constructor(
    private dialog: MatDialog,
    private productService: ProductService,
    private userService: UserService,
    private orderService: OrderService
  ) {
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit(): void {
    this.page.size = 10;
    this.page.number = 0;
    this.refreshData();
  }

  refreshData(event?: PageEvent): PageEvent {
    if (event) {
      this.page.number = event.pageIndex;
      this.page.size = event.pageSize;
    }

    if (this.sort.direction === '') {
      this.page.isSorted = false;
    } else {
      this.page.isSorted = true;
    }
    this.page.sortedBy = this.sort.active;
    this.page.dir = this.sort.direction;

    this.orderService.getAll(this.page, this.searchParam).subscribe(response => {
        this.page.fromResponse(response);
        this.dataSource = response.content;
        this.pageIndex = response.number;
        this.pageSize = response.size;
        this.length = response.totalElements;
      }
    );
    return event;
  }

  sortData(sort: Sort): void {
    this.sort = sort;
    this.refreshData();
  }

  applyFilter(event: KeyboardEvent): void {
    if (event.key === 'Enter') {
      const filterValue = (event.target as HTMLInputElement).value;
      this.refreshData();
      this.searchParam = '&search='.concat(filterValue);
      this.orderService.getAll(this.page, this.searchParam).subscribe(response => {
          this.dataSource = response.content;
        }
      );
    }
  }

  deleteOrder(order: Order) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        data: {
          message: 'delete this order'
        }
      }
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.orderService.delete(order.id).subscribe(
          () => {
            this.refreshData();
          }
        );
      }
    });
  }

  getProductsByOrderId(orderId: number) {
    this.productsMap.clear();
    this.productsMap.set(orderId, true);
  }

  hideProduct(id: number) {
    this.productsMap.set(id, false);
  }

  openProductDetails(product: Product) {
    this.dialog.open(ProductDetailsComponent, {
      data: {
        id: product.id,
        name: product.name,
        price: product.price,
        description: product.description,
        restaurantId: product.restaurantId,
        position: product.position,
        category: product.category
      }
    });
  }

  openUserDetails(userId: number) {
    let user;
    this.userService.getById(userId).subscribe(
      result => {
        user = result;
        this.dialog.open(UserDetailsComponent, {
          data: {
            id: user.id,
            name: user.name,
            secondName: user.secondName,
            email: user.email,
            address: user.address,
            phoneNumber: user.phoneNumber,
            login: user.login,
            password: user.password
          }
        });
      }
    )
  }
}
