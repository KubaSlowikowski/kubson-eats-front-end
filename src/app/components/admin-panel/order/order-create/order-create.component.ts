import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {Order} from '../../../../entities/order';
import {OrderService} from '../../../../services/order/order-service';
import {Product} from '../../../../entities/product';
import {ProductService} from '../../../../services/product/product-service';
import {Page} from '../../../../entities/page';
import {UserService} from '../../../../services/user/user-service';
import {User} from '../../../../entities/user';

@Component({
  selector: 'app-order-create',
  templateUrl: './order-create.component.html',
  styleUrls: ['./order-create.component.css']
})
export class OrderCreateComponent implements OnInit {
  createOrderForm: FormGroup;
  submitted = false;
  availableProducts: Product[];
  availableUsers: User[];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private orderService: OrderService,
    private userService: UserService,
    private productService: ProductService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.createOrderForm = this.formBuilder.group({
      orderComment: ['', [Validators.maxLength(255)]],
      userId: ['', [Validators.required, Validators.min(0)]],
      products: ['', [Validators.required]],
    });

    this.productService.getAll(new Page<Product>(1000)).subscribe(
      response => {
        this.availableProducts = response.content;
      }
    );

    this.userService.getAll(new Page<User>(1000)).subscribe(
      response => {
        this.availableUsers = response.content;
      }
    )
  }

  submitForm() {
    this.submitted = true;
    if (this.createOrderForm.invalid) {
      return;
    }
    const newOrder = this.convertFormToOrder(this.createOrderForm);
    this.orderService.save(newOrder).subscribe(
      () => {
        this.goBack();
      }
    );
  }

  convertFormToOrder(orderForm: FormGroup): Order {
    return {
      id: null,
      orderComment: orderForm.value.orderComment,
      userId: orderForm.value.userId,
      products: orderForm.value.products,
      orderStatus: null
    };
  }

  goBack() {
    this.location.back();
  }
}
