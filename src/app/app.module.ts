import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {MatToolbarModule} from '@angular/material/toolbar';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {NavbarComponent} from './components/navbar/navbar/navbar.component';
import {HomeComponent} from './components/home/home.component';
import {MatIconModule} from '@angular/material/icon';
import {ToggleMenuComponent} from './components/navbar/toggle_menu/toggle-menu/toggle-menu.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import {UserTableComponent} from './components/admin-panel/user/user-table/user-table.component';
import {UserCreateComponent} from './components/admin-panel/user/user-create/user-create.component';
import {UserUpdateComponent} from './components/admin-panel/user/user-update/user-update.component';
import {RestaurantUpdateComponent} from './components/admin-panel/restaurant/restaurant-update/restaurant-update.component';
import {RestaurantCreateComponent} from './components/admin-panel/restaurant/restaurant-create/restaurant-create.component';
import {RestaurantTableComponent} from './components/admin-panel/restaurant/restaurant-table/restaurant-table.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTableModule} from '@angular/material/table';
import {HttpClientModule} from '@angular/common/http';
import {MatSortModule} from '@angular/material/sort';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {ReactiveFormsModule} from '@angular/forms';
import {AddressDetailsComponent} from './components/admin-panel/address/address-details/address-details.component';
import {AdminPanelComponent} from './components/admin-panel/admin-panel/admin-panel.component';
import {MatCardModule} from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import {ProductCreateComponent} from './components/admin-panel/product/product-create/product-create.component';
import {ProductUpdateComponent} from './components/admin-panel/product/product-update/product-update.component';
import {OrderUpdateComponent} from './components/admin-panel/order/order-update/order-update.component';
import {OrderCreateComponent} from './components/admin-panel/order/order-create/order-create.component';
import {ProductTableComponent} from './components/admin-panel/product/product-table/product-table.component';
import {ProductDetailsComponent} from './components/admin-panel/product/product-details/product-details.component';
import {ConfirmDialogComponent} from './components/confirm-dialog/confirm-dialog.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { RestaurantDetailsComponent } from './components/admin-panel/restaurant/restaurant-details/restaurant-details.component';
import { UserDetailsComponent } from './components/admin-panel/user/user-details/user-details.component';
import {OrderTableComponent} from './components/admin-panel/order/order-table/order-table.component';
import { RestaurantListComponent } from './components/main/restaurants/restaurant-list/restaurant-list.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    ToggleMenuComponent,
    UserTableComponent,
    UserCreateComponent,
    UserUpdateComponent,
    RestaurantUpdateComponent,
    RestaurantCreateComponent,
    RestaurantTableComponent,
    AddressDetailsComponent,
    AdminPanelComponent,
    ProductTableComponent,
    ProductCreateComponent,
    ProductUpdateComponent,
    OrderUpdateComponent,
    OrderCreateComponent,
    OrderTableComponent,
    ProductDetailsComponent,
    ConfirmDialogComponent,
    RestaurantDetailsComponent,
    UserDetailsComponent,
    RestaurantListComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([
      {path: 'home', component: HomeComponent},
      {path: '', redirectTo: 'home', pathMatch: 'full'},

      {path: 'admin-panel', component: AdminPanelComponent},

      {path: 'admin-panel/users', component: UserTableComponent},
      {path: 'admin-panel/users/add', component: UserCreateComponent},
      {path: 'admin-panel/users/update/:id', component: UserUpdateComponent},

      {path: 'admin-panel/orders', component: OrderTableComponent},
      {path: 'admin-panel/orders/add', component: OrderCreateComponent},
      {path: 'admin-panel/orders/update/:id', component: OrderUpdateComponent},

      {path: 'admin-panel/restaurants', component: RestaurantTableComponent},
      {path: 'admin-panel/restaurants/add', component: RestaurantCreateComponent},
      {path: 'admin-panel/restaurants/update/:id', component: RestaurantUpdateComponent},

      {path: 'admin-panel/products', component: ProductTableComponent},
      {path: 'admin-panel/products/add', component: ProductCreateComponent},
      {path: 'admin-panel/products/update/:id', component: ProductUpdateComponent},

      {path: 'restaurants', component: RestaurantListComponent},

    ]),
    MatIconModule,
    MatDialogModule,
    MatMenuModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatTableModule,
    HttpClientModule,
    MatSortModule,
    MatInputModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatCardModule,
    MatGridListModule,
    MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
