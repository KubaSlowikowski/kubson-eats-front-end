import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Page} from '../../entities/page';
import {Observable} from 'rxjs';
import {Order} from '../../entities/order';
import {AppSettings} from "../../utils/app-settings";

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private readonly API_URL: string;

  constructor(private httpClient: HttpClient) {
    this.API_URL = AppSettings.API_ENDPOINT + '/orders'
  }

  public getAll(page: Page<Order>, search: string = ''): Observable<Page<Order>> {
    return this.httpClient.get<Page<Order>>(this.API_URL + page.buildPageableUrl() + search, {withCredentials: true});
  }

  public getById(id: number): Observable<Order> {
    return this.httpClient.get<Order>(this.API_URL + '/' + id, {withCredentials: true});
  }

  public save(order: Order): Observable<Order> {
    return this.httpClient.post<Order>(this.API_URL, order, {withCredentials: true});
  }

  public update(order: Order): Observable<Order> {
    return this.httpClient.put<Order>(this.API_URL + '/' + order.id, order, {withCredentials: true});
  }

  public delete(id: number): Observable<Order> {
    return this.httpClient.delete<Order>(this.API_URL + '/' + id);
  }
}
