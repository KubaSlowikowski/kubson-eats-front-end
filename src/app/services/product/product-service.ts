import {HttpClient} from '@angular/common/http';
import {Page} from '../../entities/page';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {Product} from '../../entities/product';
import {AppSettings} from "../../utils/app-settings";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private readonly API_URL: string;

  constructor(private httpClient: HttpClient) {
    this.API_URL = AppSettings.API_ENDPOINT + '/products'
  }

  public getAll(page: Page<Product>, search: string = ''): Observable<Page<Product>> {
    return this.httpClient.get<Page<Product>>(this.API_URL + page.buildPageableUrl() + search, {withCredentials: true});
  }

  public getById(id: number): Observable<Product> {
    return this.httpClient.get<Product>(this.API_URL + '/' + id, {withCredentials: true});
  }

  public save(product: Product): Observable<Product> {
    return this.httpClient.post<Product>(this.API_URL, product, {withCredentials: true});
  }

  public update(product: Product): Observable<Product> {
    return this.httpClient.put<Product>(this.API_URL + '/' + product.id, product, {withCredentials: true});
  }

  public delete(id: number): Observable<Product> {
    return this.httpClient.delete<Product>(this.API_URL + '/' + id);
  }

  public getByRestaurantId(page: Page<Product>, id: number): Observable<Page<Product>> {
    return this.httpClient.get<Page<Product>>(this.API_URL + '/findByRestaurantId/' + id + page.buildPageableUrl(), {withCredentials: true})
  }

  public getByOrderId(page: Page<Product>, id: number): Observable<Page<Product>> {
    return this.httpClient.get<Page<Product>>(this.API_URL + '/findByOrderId/' + id + page.buildPageableUrl(), {withCredentials: true})
  }
}
