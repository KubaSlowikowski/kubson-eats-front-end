import {HttpClient} from '@angular/common/http';
import {Page} from '../../entities/page';
import {Observable} from 'rxjs';
import {Restaurant} from '../../entities/restaurant';
import {Injectable} from '@angular/core';
import {AppSettings} from "../../utils/app-settings";

@Injectable({
  providedIn: 'root',
})
export class RestaurantService {

  private readonly API_URL: string;

  constructor(private httpClient: HttpClient) {
    this.API_URL = AppSettings.API_ENDPOINT + '/restaurants'
  }

  public getAll(page: Page<Restaurant>, search: string = ''): Observable<Page<Restaurant>> {
    return this.httpClient.get<Page<Restaurant>>(this.API_URL + page.buildPageableUrl() + search, {withCredentials: true});
  }

  public getById(id: number): Observable<Restaurant> {
    return this.httpClient.get<Restaurant>(this.API_URL + '/' + id, {withCredentials: true});
  }

  public save(restaurant: Restaurant): Observable<Restaurant> {
    return this.httpClient.post<Restaurant>(this.API_URL, restaurant, {withCredentials: true});
  }

  public update(restaurant: Restaurant): Observable<Restaurant> {
    return this.httpClient.put<Restaurant>(this.API_URL + '/' + restaurant.id, restaurant, {withCredentials: true});
  }

  public delete(id: number): Observable<Restaurant> {
    return this.httpClient.delete<Restaurant>(this.API_URL + '/' + id);
  }
}
