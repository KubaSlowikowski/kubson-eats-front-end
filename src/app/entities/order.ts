import {Product} from './product';

export class Order {
  id: number;
  orderComment: string;
  userId: number;
  products: Product[];
  orderStatus: string;
}
