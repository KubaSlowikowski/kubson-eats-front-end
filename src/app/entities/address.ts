export class Address {
  id: number;
  country: string;
  city: string;
  street: string;
  buildingNumber: number;
  apartmentNumber: number;
  zipCode: string;
}
