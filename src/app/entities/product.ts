export class Product {
  id: number;
  name: string;
  price: number;
  description: string;
  restaurantId: number;
  position: number;
  category: string;
}
