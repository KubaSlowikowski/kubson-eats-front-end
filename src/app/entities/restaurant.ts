import {Address} from './address';
import {Product} from './product';

export class Restaurant {
  id: number;
  name: string;
  category: string;
  address: Address;
  phoneNumber: string;
  email: string;
  login: string;
  password: string;
  products: Product[];

}
