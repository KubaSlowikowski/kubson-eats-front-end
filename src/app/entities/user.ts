import {Address} from './address';

export class User {
  id: number;
  name: string;
  secondName: string;
  email: string;
  address: Address;
  phoneNumber: string;
  login: string;
  password: string;
}
